//Макс
val myList:List[BigInt] = List(1,5,12,3)

def myMax(lst:List[BigInt] = List()  ,max:BigInt = 0):BigInt = {

  //val maxVal:BigInt = if (max>lst.head) max else lst.head;
  if(lst.isEmpty) max else {
    myMax(lst.tail,if (max>lst.head) max else lst.head)
  }

}

myMax(myList,0)
myMax()

