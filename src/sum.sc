// сумма всех элементов коллекции
val myList:List[BigInt] = List(1,5,12,3)
def m1sum(l: List[BigInt]): BigInt = l.reduce(_ + _)
m1sum(myList)

//myList.length
//myList.tail
//myList.head

def mySum(lst: List[BigInt], sum: BigInt): BigInt = {

  if (lst.isEmpty) sum else {
    mySum(lst.tail, sum + lst.head)
  }
}

mySum(myList, 0)

def inSum(args: BigInt*): Unit = {
  for (i <- args) print(i)
  println(mySum(args.toList, 0))
}

inSum(4,4,4)









