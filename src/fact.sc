//1
//def myFunc(i:Int):Int = {
//  if (i > 0) i else -i
//}
//myFunc(5)
//myFunc(-5)

//Простой факториал
//def fact(i:BigInt):BigInt = {
//  if(i<=0) 1 else {
//    i*fact(i-1)
//  }
//}
//fact(3)

import scala.BigInt._
import scala.annotation.tailrec

//Хвостовой факториал
def fact(i:BigInt):BigInt = {

  @tailrec
  def factX(n:BigInt,acc:BigInt):BigInt = {

    if(n==0) acc else {
      //printf("n:%d,acc:%d",n,acc)
      factX(n-1,acc*n)
    }
  }

  if(i<=0) 1 else {
    factX(i,1)
  }
}
//2+2
fact(30000)



//var my=BigInt("2323212225")

//fact(BigInt(2323212225))




//fact(BigInt("4323423423"))










//val myList = List(15,232,41,1,3)
//myList.max



