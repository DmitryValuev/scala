import scala.io.Source._

object HelloWorld {
  def main(args: Array[String]): Unit = {
     println("Hello, world!!!!!!!!");
    val readmeText : Iterator[String] = fromResource("readme.txt").getLines
    //val readmeText : Iterator[String] = fromFile("E:\\Scala\\Projects\\src\\main\\resources\\readme.txt").getLines
    val myList : List[String] = readmeText.toList
    myList.foreach((line:String) => println(line))
    //throw new IllegalArgumentException("Плохой аргумент")
  }
}
