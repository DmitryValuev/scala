def myPow(x:BigInt,n:BigInt):BigInt = {

  if (n == 1) x else x*myPow(x,n-1)

}

myPow(5,4)


